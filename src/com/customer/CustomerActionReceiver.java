package com.customer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.cxb.carrecorder.PreviewService;

/**
 * Author jerry
 * date: 2016/7/4.
 */
public class CustomerActionReceiver extends BroadcastReceiver {
	private static final String TAG = "debug";
	private Context mContext;
	public CustomerActionReceiver(Context context){
		super();
		mContext = context;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e(TAG, "onReceiver:"+intent.getAction());
		String action = intent.getAction();
		Intent in = new Intent();
		in.setClassName("com.cxb.carrecorder", "com.cxb.carrecorder.PreviewService");
		in.putExtra(CustomerConfig.REQUEST_FROM_OTHER_APP, true);

		if(action.equals(CustomerConfig.ACTION_REQUEST_PICTURE)){
			//随拍
			in.setAction(PreviewService.ACTION_GET_PICTURE_SHARE);
		}else if(action.equals(CustomerConfig.ACTION_REQUEST_VIDEO)){
			//随录
			in.setAction(PreviewService.ACTION_GET_VIDEO_SHARE);
		}else if(action.equals(CustomerConfig.ACTION_REQUEST_LIVE)){
			//直播
			long time = intent.getLongExtra(CustomerConfig.LIVE_TIME, -1);
			String id = intent.getStringExtra(CustomerConfig.LIVE_DEVICE_ID);
			String subject = intent.getStringExtra(CustomerConfig.LIVE_SUBJECT);
			String pushUrl = intent.getStringExtra(CustomerConfig.LIVE_PUSH_URL);
			if(time <= 0 || TextUtils.isEmpty(pushUrl)){
				return;
			}

			in.setAction(PreviewService.ACTION_LIVE_SHOW);
			in.putExtra(CustomerConfig.LIVE_TIME, time);
			in.putExtra(CustomerConfig.LIVE_DEVICE_ID, id);
			in.putExtra(CustomerConfig.LIVE_SUBJECT, subject);
			in.putExtra(CustomerConfig.LIVE_PUSH_URL, pushUrl);
		}else if(action.equals(CustomerConfig.ACTION_REQUEST_STOP_LIVE)){
			in.setAction(PreviewService.ACTION_STOP_LIVE_SHOW);
		}else if(action.equals(CustomerConfig.ACTION_REQUEST_DEVICEID)){
			return;
		} else {
			Log.e(TAG, "UNKNOW ACTION:"+intent.getAction());
		}
		Log.e(TAG, "startService for action: "+in.getAction());
		mContext.startService(in);
	}
}
