package com.customer;

/**
 * Author jerry
 * date: 2016/7/4.
 */
public class CustomerConfig {
	public static final String ACTION_REQUEST_PICTURE = "dvr.intent.action.REQUEST_PICTURE";
	public static final String RESULT_PICTURE = "dvr.intent.action.RESULT_PICTURE";

	public static final String ACTION_REQUEST_VIDEO = "dvr.intent.action.REQUEST_VIDEO";
	public static final String RESULT_VIDEO = "dvr.intent.action.RESULT_VIDEO";

	public static final String ACTION_REQUEST_LIVE = "dvr.intent.action.REQUEST_LIVE";
	public static final String ACTION_REQUEST_STOP_LIVE = "dvr.intent.action.REQUEST_STOP_LIVE";
	public static final String RESULT_LIVE = "dvr.intent.action.RESULT_LIVE";

	public static final String RESULT_BUSY = "dvr.too.much.tasks";
	public static final String REQUEST_FROM_OTHER_APP = "third_app";

	public static final String ACTION_REQUEST_DEVICEID = "dvr.intent.action.REQUEST_DEVICEID";
	public static final String RESULT_DEVICEID = "dvr.intent.action.RESULT_DEVICEID";


	public static final String CODE = "code";
	public static final String PATH = "path";
	public static final String DURATION = "duration";
	public static final String PICTURE_TIME = "take_time";
	public static final String VIDEO_START_TIME = "start_time";
	public static final String VIDEO_END_TIME = "end_time";


	public static final String LIVE_TIME = "duration";//直播时长
	public static final String LIVE_PUSH_URL = "pushUrl";//推流地址
	public static final String LIVE_DEVICE_ID = "deviceid";//设备ID
	public static final String LIVE_SUBJECT = "subject";//主题

	public static final int LIVE_CREATE_SUCCESS = 0;
	public static final int LIVE_CREATE_FAIL = 1;
	public static final int LIVE_PUSH_FAIL = 2;
	public static final int LIVE_ENT = 3;

	public static final int STATUS_UNKNOW_ERROR = -1;//未知错误
	public static final int STATUS_TAKE_PICTURE = 0;//在拍照
	public static final int STATUS_TAKE_VIDEO = 1;//在录像
	public static final int STATUS_MAKE_LIVE = 2;//在直播

}
