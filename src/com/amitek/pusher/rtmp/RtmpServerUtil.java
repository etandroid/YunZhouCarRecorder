package com.amitek.pusher.rtmp;

import android.net.Uri;
import android.util.Log;
import org.json.JSONObject;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * RTMP服务器操作类
 */
public class RtmpServerUtil {
	private static final String TAG = RtmpServerUtil.class.getSimpleName();

	private static final String LIVE_CREATE_URL_LETV = "http://service.ami-tek.com.cn/live/create";
	private static final String LIVE_STOP_URL_LETV = "http://service.ami-tek.com.cn/live/stop";

	/**
	 * 创建LETV的视频直播活动. 该函数不能在UI线程调用, 必须在后台线程调用.
	 * @param deviceId     设备ID
	 * @param activityName 视频直播活动的名称. 如果为null或空, 由系统自动生成.
	 * @param duration     视频直播活动的持续时间. 单位为: 分钟. 如果小于等于0, 则缺省为1天.
	 * @return 生成的LETV的视频直播活动的RTMP推流地址URL
	 * @throws Throwable 执行错误
	 */
	public static String createLiveBroadcastLetv(String deviceId, String activityName, long duration)
			throws Throwable {
		try {
			// 设置请求参数
			RequestParams params = new RequestParams(LIVE_CREATE_URL_LETV);
			params.setMethod(HttpMethod.POST);
			params.addBodyParameter("deviceId", deviceId);
			params.addBodyParameter("activityName", activityName);
			if (duration > 0) params.addBodyParameter("duration", Long.toString(duration));
			Log.d(TAG, "Requesting create live broadcast url: " + params.toString());
			// 获取请求应答
			String response = x.http().postSync(params, String.class);
			// 处理应答数据
			try {
				if (response == null || response.isEmpty()) {
					throw new Exception("empty json.");
				}
				JSONObject json = new JSONObject(response);
				if (json.has("code")) {
					throw new Exception("error code:" + json.get("code") + ".");
				}
				String activityId = json.getString("activityId");
				String pushUrl = json.getJSONArray("lives").getJSONObject(0).getString("pushUrl") + "&activityId=" + activityId;
				Log.i(TAG, "Requesting create live broadcast return: " + pushUrl);
				return pushUrl;
			} catch (Throwable ex) {
				throw new Exception("Parse response json failed, response:" + response, ex);
			}
		} catch (Throwable ex) {
			//TODO
			Log.e(TAG, "Request create live broadcast error!", ex);
			throw ex;
		}
	}

	/**
	 * 停止LETV的视频直播活动. 该函数不能在UI线程调用, 必须在后台线程调用.
	 * @param pushUrl LETV的视频直播活动的RTMP推流地址URL
	 * @return true - 成功; false - 失败
	 */
	public static boolean removeLiveBroadcastLetv(String pushUrl) {
		try {
			// 设置请求参数
			RequestParams params = new RequestParams(LIVE_STOP_URL_LETV);
			params.setMethod(HttpMethod.POST);
			String activityId = Uri.parse(pushUrl).getQueryParameter("activityId");
			params.addBodyParameter("activityId", activityId);
			Log.d(TAG, "Requesting stop live broadcast url: " + params.toString());
			// 获取请求应答
			String response = x.http().postSync(params, String.class);
			// 处理应答数据
			try {
				if (response == null || response.isEmpty()) {
					throw new Exception("empty json.");
				}
				JSONObject json = new JSONObject(response);
				if (json.getInt("code") != 200) {
					throw new Exception("error code:" + json.get("code") + ".");
				}
			} catch (Throwable ex) {
				throw new Exception("Parse response json failed, response:" + response, ex);
			}
			Log.i(TAG, "Requesting stop live broadcast return: " + response);
			return true;
		} catch (Throwable ex) {
			Log.e(TAG, "Request stop broadcast error!", ex);
			return false;
		}
	}
}
