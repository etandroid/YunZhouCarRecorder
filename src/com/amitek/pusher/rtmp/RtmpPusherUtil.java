package com.amitek.pusher.rtmp;

import android.content.Context;
import android.util.Log;
import com.voiceengine.NTAudioRecord;
import org.easydarwin.easypublisher.EasyPublisherJni;

/** RTMP Pusher SDK接口类 */
public class RtmpPusherUtil {
	private static final String TAG = RtmpPusherUtil.class.getSimpleName();
	// 内部常量
	private static final int CAMERA_TYPE = 2;					// (无用的参数)摄像头类型
	private static final int AUDIO_RECORD_ID = 1;				// (无用的参数)录音过程的ID
	private static final int AUDIO_SAMPLE_RATE = 44100;			// 录音采样频率
	// 内部变量
	private static EasyPublisherJni videoPusher;
	private static NTAudioRecord audioRecorder;
	private static int videoOrientation;
	private static boolean isPublishing;

	/**
	 * 初始化RTMP Pusher库. 必须先初始化才能执行RTMP pusher的功能.
	 * @param context Context对象
	 * @return true - 成功; false - 失败
	 */
	synchronized
	public static boolean initialize(Context context) {
		if (videoPusher == null || audioRecorder == null) {
			try {
				System.load("librtmppusher.so");
				//System.loadLibrary("rtmppusher");
				videoPusher = new EasyPublisherJni();
				audioRecorder = new NTAudioRecord(AUDIO_RECORD_ID);
			} catch (Throwable ex) {
				Log.e("ACTION_LIVE_SHOW", "Can not load librtmppusher.so!", ex);
			}
		}
		return isInitialized();
	}

	/**
	 * 开始推送视频和音频到RTMP服务器. 如果视频推送已经启动, 则自动停止推送并重新启动推送. 该函数可以重复调用, 调用后使用最新的参数. 如果视频推送已经启动, 则自动停止推送.
	 * @param pushUrl RTMP服务器的地址
	 * @param imageWidth  预览帧图像宽度
	 * @param imageHeight 预览帧图像高度
	 * @param orientation 图像方向. ORIENTATION_PORTRAIT - 竖屏(当前预览帧宽度/高度切换); ORIENTATION_LANDSCAPE - 横屏(保持当前预览帧宽度/高度)
	 * @param channel     音频轨道数. 1 - 单声道; 2 - 立体声; 其他 - 无伴音
	 * @return true - 成功; false - 失败
	 */
	synchronized
	public static boolean startPublish(String pushUrl, int imageWidth, int imageHeight, int orientation, int channel) {
		// 验证是否初始化和参数合法性
		if (!isInitialized() || pushUrl == null || pushUrl.isEmpty() || imageWidth <= 0 || imageHeight <= 0) {
			Log.w("ACTION_LIVE_SHOW", "startPublish() failed for invalid parameters! (pushUrl=" + pushUrl + ",imageWidth=" + imageWidth
					   + ",imageHeight=" + imageHeight + ",orientation=" + orientation + ",channel=" + channel + ")");
			return false;
		}
		// 如果正在推送视频, 则自动停止推送
		if (isPublishing()) {
			stopPublish();
		}
		// 设置视频推送参数
		int rt = videoPusher.EasyPublisherInit(imageWidth, imageHeight);
		if (rt != 0) {
			Log.w("ACTION_LIVE_SHOW", "startPublish() failed for EasyPublisherInit(" + imageWidth + "," + imageHeight + ") return " + rt + "!");
			return false;
		}
		// 启动视频推送
		videoOrientation = orientation;
		rt = videoPusher.EasyPublisherStartPublish(pushUrl);
		if (rt != 0) {
			Log.w("ACTION_LIVE_SHOW", "startPublish() failed for EasyPublisherStartPublish(" + pushUrl + ") return " + rt + "!");
			return false;
		}
		// 启动音频录音
		if (channel == 1 || channel == 2) {
			if (!audioRecorder.startRecording(AUDIO_SAMPLE_RATE, channel)) {
				Log.w("ACTION_LIVE_SHOW", "Start audio recorder failed, publish broadcast without audio!");
			}
		}
		// 设置当前状态
		isPublishing = true;
		Log.e("ACTION_LIVE_SHOW", "startPublish(pushUrl=" + pushUrl + ",imageWidth=" + imageWidth + ",imageHeight=" + imageHeight
				   + ",orientation=" + orientation + ",channel=" + channel+ ") success.");
		return true;
	}

	/**
	 * 停止推送视频和音频到RTMP服务器.
	 */
	synchronized
	public static void stopPublish() {
		if (isPublishing()) {
			audioRecorder.stopRecording();
			videoPusher.EasyPublisherStopPublish();
			isPublishing = false;
		}
	}

	/**
	 * 推送图像数据到RTMP服务器. 一般情况下, 图像数据是从摄像头预览帧得到的.
	 * @param data 图像数据缓冲区
	 * @param len  图像数据长度
	 * @return true - 成功; false - 失败
	 */
	public static boolean publishImage(byte[] data, int len) {
		if (!isPublishing()) {
			return false;
		}
		videoPusher.EasyPublisherOnCaptureVideoData(data, len, CAMERA_TYPE, videoOrientation);
		return true;
	}

	/**
	 * 返回RTMP Pusher库是否已经成功初始化
	 * @return true - 已经成功初始化; false - 未成功初始化
	 */
	public static boolean isInitialized() {
		return videoPusher != null && audioRecorder != null;
	}

	/**
	 * 返回RTMP Pusher库是否正在推送视频
	 * @return true - 正在推送视频; false - 未在推送视频
	 */
	public static boolean isPublishing() {
		return isPublishing;
	}
}
