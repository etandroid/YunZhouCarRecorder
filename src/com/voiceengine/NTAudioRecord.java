package com.voiceengine;

import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AudioEffect.Descriptor;
import android.os.Build.VERSION;
import android.os.Process;
import android.util.Log;

import java.nio.ByteBuffer;

import static android.media.AudioFormat.*;

/**
 * 录音类定义. 此类被Native库调用, 不能改变包名和类名
 */
public class NTAudioRecord {
	private static final String TAG = NTAudioRecord.class.getSimpleName();
	private static final int BITS_PER_SAMPLE = 16;
	private static final int BUFFERS_PER_SECOND = 100;

	private final long recordId;
	private ByteBuffer byteBuffer = null;
	private AudioRecord audioRecord = null;
	private AudioRecordThread audioThread = null;
	private AcousticEchoCanceler aecObject = null;

	/**
	 * 构造函数
	 * @param recordId 录音过程的Id
	 */
	public NTAudioRecord(long recordId) {
		Log.d(TAG, "ctor" + NTAudioUtils.getThreadInfo() + "\n" + NTAudioUtils.getDeviceInfo());
		this.recordId = recordId;
	}

	/**
	 * 启动录音
	 * @param sampleRate 采样频率
	 * @param channels   声道数量
	 * @return true - 成功; false - 失败
	 */
	public boolean startRecording(int sampleRate, int channels) {
		Log.d(TAG, "startRecording(sampleRate=" + sampleRate + ", channels=" + channels + ")");
		if (initRecording(sampleRate, channels) <= 0) {
			Log.e(TAG, "initRecording failed!");
			return false;
		}
		if (!executeRecording()) {
			Log.e(TAG, "executeRecording failed!");
			return false;
		}
		return true;
	}

	/**
	 * 停止录音
	 * @return true - 成功; false - 失败
	 */
	public boolean stopRecording() {
		Log.d(TAG, "stopRecording");
		if (audioThread != null) {
			audioThread.joinThread();
			audioThread = null;
		}
		if (aecObject != null) {
			aecObject.release();
			aecObject = null;
		}
		if (audioRecord != null) {
			audioRecord.release();
			audioRecord = null;
		}
		return true;
	}

	/**
	 * 打开/关闭内置的AEC降噪
	 * @param enable 打开/关闭内置的AEC降噪
	 * @return true - 成功; false - 失败
	 */
	private boolean enableBuiltInAEC(boolean enable) {
		Log.d(TAG, "enableBuiltInAEC(" + enable + ')');
		assertTrue(NTAudioUtils.isAcousticEchoCancelerApproved());
		if (aecObject == null) return true;
		if (aecObject.setEnabled(enable) != 0) {
			Log.e(TAG, "AcousticEchoCanceler.setEnabled failed");
			return false;
		} else {
			Log.d(TAG, "AcousticEchoCanceler.getEnabled: " + aecObject.getEnabled());
			return true;
		}
	}

	// Native推流库函数: 设置录音缓冲区地址
	private native void nativeCacheDirectBufferAddress(ByteBuffer buffer, long recordId);

	// Native推流库函数: 将录音缓存区内容编码发送到推流服务器
	private native void nativeDataIsRecorded(int count, long recordId);

	// 初始化录音
	private int initRecording(int sampleRate, int channels) {
		Log.d(TAG, "initRecording(sampleRate=" + sampleRate + ", channels=" + channels + ")");
		// 分配录音缓冲区
		int channelConfig = channels != 2 ? CHANNEL_IN_MONO : CHANNEL_IN_STEREO;
		int bytesPerFrame = channels * (BITS_PER_SAMPLE / 8);
		int framesPerBuffer = sampleRate / BUFFERS_PER_SECOND;
		byteBuffer = ByteBuffer.allocateDirect(bytesPerFrame * framesPerBuffer);
		Log.d(TAG, "byteBuffer.capacity: " + byteBuffer.capacity() + "; framesPerBuffer: " + framesPerBuffer);
		// 设置推流库的录音缓冲区
		nativeCacheDirectBufferAddress(byteBuffer, recordId);
		// 释放之前创建的AEC对象
		if (aecObject != null) {
			aecObject.release();
			aecObject = null;
		}
		// 创建AudioRecord对象
		assertTrue(audioRecord == null);
		int minBufferSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, ENCODING_PCM_16BIT);
		int bufferSizeInBytes = Math.max(byteBuffer.capacity(), minBufferSize);
		Log.d(TAG, "AudioRecord.getMinBufferSize: " + minBufferSize + ", BufferSizeInBytes: " + bufferSizeInBytes);
		try {
			if(chkNewDev()) {
				Log.d(TAG, "check new Dev with true");
				audioRecord = new AudioRecord(AudioSource.VOICE_COMMUNICATION, sampleRate, channelConfig, ENCODING_PCM_16BIT, bufferSizeInBytes);
			} else {
				Log.d(TAG, "check new Dev with false");
				audioRecord = new AudioRecord(AudioSource.MIC, sampleRate, channelConfig, ENCODING_PCM_16BIT, bufferSizeInBytes);
			}
		} catch (IllegalArgumentException ex) {
			Log.e(TAG, "Create AudioRecord failed!", ex);
			return -1;
		}
		assertTrue(audioRecord.getState() == AudioRecord.STATE_INITIALIZED);
		Log.d(TAG, "AudioRecord session ID: " + audioRecord.getAudioSessionId() + ", "
				   + "audio format: " + audioRecord.getAudioFormat() + ", "
				   + "channels: " + audioRecord.getChannelCount() + ", "
				   + "sample rate: " + audioRecord.getSampleRate());
		// 创建AEC对象
		Log.d(TAG, "AcousticEchoCanceler.isAvailable: " + builtInAECIsAvailable());
		if (builtInAECIsAvailable()) {
			int audioSession = audioRecord.getAudioSessionId();
			Log.d(TAG, "audioSession with: " + audioSession);
			aecObject = AcousticEchoCanceler.create(audioSession);
			if (aecObject == null) {
				Log.w(TAG, "AcousticEchoCanceler.create failed!");
			} else if (aecObject.setEnabled(true) != 0) {
				Log.w(TAG, "AcousticEchoCanceler.setEnabled failed");
			} else {
				Descriptor descriptor = aecObject.getDescriptor();
				Log.d(TAG, "AcousticEchoCanceler name: " + descriptor.name + ", " + "implementor: " + descriptor.implementor + ", " + "uuid: " + descriptor.uuid);
				Log.d(TAG, "AcousticEchoCanceler.getEnabled: " + aecObject.getEnabled());
			}
		}
		return framesPerBuffer;
	}

	// 启动录音
	private boolean executeRecording() {
		Log.d(TAG, "startRecording");
		assertTrue(audioRecord != null);
		assertTrue(audioThread == null);
		// 启动录音
		try {
			audioRecord.startRecording();
		} catch (IllegalStateException ex) {
			Log.e(TAG, "AudioRecord.startRecording failed!", ex);
			return false;
		}
		// 判断是否启动成功
		if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
			Log.e(TAG, "AudioRecord.startRecording failed");
			return false;
		}
		// 创建录音线程并启动
		audioThread = new AudioRecordThread(AudioRecordThread.class.getSimpleName());
		audioThread.start();
		return true;
	}

	// 判断是否支持内置的AEC降噪
	private static boolean builtInAECIsAvailable() {
		return NTAudioUtils.isAcousticEchoCancelerSupported();
	}

	// 是否支持新调用接口
	private static boolean chkNewDev() {
		return VERSION.SDK_INT >= 16;
	}

	// ASSERT工具函数
	private static void assertTrue(boolean condition) {
		if (!condition) {
			throw new AssertionError("Expected condition to be true");
		}
	}

	// 录音线程
	private class AudioRecordThread extends Thread {
		private volatile boolean keepAlive = true;

		public AudioRecordThread(String name) {
			super(name);
		}

		public void run() {
			Process.setThreadPriority(-19);
			Log.d(TAG, "AudioRecordThread" + NTAudioUtils.getThreadInfo());
			NTAudioRecord.assertTrue(audioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING);

			while (keepAlive) {
				int readBytes = audioRecord.read(byteBuffer, byteBuffer.capacity());
				if (readBytes != byteBuffer.capacity()) {
					Log.e(TAG, "AudioRecord.read failed: " + readBytes);
					if (readBytes == AudioRecord.ERROR_INVALID_OPERATION) {
						keepAlive = false;
					}
				} else {
					nativeDataIsRecorded(readBytes, recordId);
				}
			}

			try {
				audioRecord.stop();
			} catch (IllegalStateException ex) {
				Log.w(TAG, "AudioRecord.stop failed!", ex);
			}
		}

		public void joinThread() {
			keepAlive = false;
			while(isAlive()) {
				try {
					join();
				} catch (InterruptedException ex) {
				}
			}
		}
	}
}
