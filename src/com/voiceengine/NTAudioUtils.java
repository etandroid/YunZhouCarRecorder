package com.voiceengine;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;

import java.util.Arrays;
import java.util.List;

/** 录音/放音工具类 */
public final class NTAudioUtils {
	private static final List<String> BLACKLISTED_AEC_MODELS = Arrays.asList("Nexus 5", "D6503");
	private static final List<String> BLACKLISTED_OPEN_SL_ES_MODELS = Arrays.asList("Nexus 6");
	private static final int SAMPLE_RATE_HZ = 44100;

	/**
	 * 返回是否运行在Android2.3以上版本
	 * @return true - 是; false - 否
	 */
	public static boolean runningOnGingerBreadOrHigher() {
		return Build.VERSION.SDK_INT >= 9;
	}

	/**
	 * 返回是否运行在Android4.1以上版本
	 * @return true - 是; false - 否
	 */
	public static boolean runningOnJellyBeanOrHigher() {
		return Build.VERSION.SDK_INT >= 16;
	}

	/**
	 * 返回是否运行在Android4.2以上版本
	 * @return true - 是; false - 否
	 */
	public static boolean runningOnJellyBeanMR1OrHigher() {
		return Build.VERSION.SDK_INT >= 17;
	}

	/**
	 * 返回是否运行在Android5.0以上版本
	 * @return true - 是; false - 否
	 */
	public static boolean runningOnLollipopOrHigher() {
		return Build.VERSION.SDK_INT >= 21;
	}

	/**
	 * 返回是否运行在模拟器上
	 * @return true - 是; false - 否
	 */
	public static boolean runningOnEmulator() {
		return Build.HARDWARE.equals("goldfish") && Build.BRAND.startsWith("generic_");
	}

	/**
	 * 返回当前设备是否在不使用内置硬件AEC降噪的名单内
	 * @return true - 是; false - 否
	 */
	public static boolean deviceIsBlacklistedForHwAecUsage() {
		return BLACKLISTED_AEC_MODELS.contains(Build.MODEL);
	}

	/**
	 * 返回当前设备是否在不使用OpenSL/ES的名单内
	 * @return true - 是; false - 否
	 */
	public static boolean deviceIsBlacklistedForOpenSLESUsage() {
		return BLACKLISTED_OPEN_SL_ES_MODELS.contains(Build.MODEL);
	}

	/**
	 * 返回当前设备支持内置硬件AEC降噪
	 * @return true - 是; false - 否
	 */
	public static boolean isAcousticEchoCancelerSupported() {
		return runningOnJellyBeanOrHigher();
	}

	/**
	 * 返回当前设备可以使用内置硬件AEC降噪
	 * @return true - 是; false - 否
	 */
	public static boolean isAcousticEchoCancelerApproved() {
		return !deviceIsBlacklistedForHwAecUsage() && isAcousticEchoCancelerSupported();
	}

	/**
	 * 返回当前APK是否具有指定权限
	 * @return true - 是; false - 否
	 */
	public static boolean hasPermission(Context context, String permission) {
		return context.checkPermission(permission, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
	}

	/**
	 * 返回指定线程的信息
	 * @return 线程信息字符串
	 */
	public static String getThreadInfo() {
		return "@[name=" + Thread.currentThread().getName() + ", id=" + Thread.currentThread().getId() + "]";
	}

	/**
	 * 返回当前设备的信息
	 * @return 设备信息字符串
	 */
	public static String getDeviceInfo() {
		return "Android SDK: " + Build.VERSION.SDK_INT + ", "
			   + "Release: " + Build.VERSION.RELEASE + ", "
			   + "Brand: " + Build.BRAND + ", "
			   + "Device: " + Build.DEVICE + ", "
			   + "Id: " + Build.ID + ", "
			   + "Hardware: " + Build.HARDWARE + ", "
			   + "Manufacturer: " + Build.MANUFACTURER + ", "
			   + "Model: " + Build.MODEL + ", "
			   + "Product: " + Build.PRODUCT;
	}
}
