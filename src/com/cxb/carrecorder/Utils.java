package com.cxb.carrecorder;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.lang.reflect.Method;

public class Utils {
	
	public static void playTts(Context context, String text, boolean isNeedToast) {

		if (isNeedToast) {
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
		}
		Intent intent = new Intent("action.cxb.app.tts");
		intent.putExtra("data", text);
		context.sendBroadcast(intent);
	}
	public static String getSystemProperties(String field)// ȡϵͳ����
	{
		String platform = null;
		try {
			Class<?> classType = Class.forName("android.os.SystemProperties");
			Method getMethod = classType.getDeclaredMethod("get",
					new Class<?>[] { String.class });
			platform = (String) getMethod.invoke(classType,
					new Object[] { field });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return platform;
	}
	
	public static boolean isMT6572(){
		return "MT6572".equals(getSystemProperties("ro.mediatek.platform"));
	}
	public static boolean isPowerOffing() {
		return "1".equals(getSystemProperties("sys.ami.poweroff"));
	}
	
	public static boolean getAdasSupport(){
		return "1".equals(getSystemProperties("cxb.with.adas"));
	}

//	public static void showToast(Context context, String msg){
//		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//	}
}
