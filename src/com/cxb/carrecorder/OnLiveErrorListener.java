package com.cxb.carrecorder;

/**
 * Author jerry
 * date: 2016/7/4.
 */
public interface OnLiveErrorListener {
	public void onCreateFail();
	public void onDeleteFail();
	public void onDeleteError();
	public void onDeleteSuccess();
	public void onCreateSuccess();
	public void onPushFail();
	public void onUserCreateFail();
}
