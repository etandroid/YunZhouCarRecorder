package com.cxb.carrecorder;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FolderActivity extends Activity {
	private TextView lockedButton = null;
	private TextView videoButton = null;
	private Button returnButton = null;
	private ImageView lockedArrow = null;
	private ImageView videoArrow = null;
	
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//		setContentView(R.layout.folderlist_activity);
//
//		lockedButton = (TextView)findViewById(R.id.locked_button);
//		videoButton = (TextView)findViewById(R.id.video_button);
//		returnButton = (Button)findViewById(R.id.return_button);
//		lockedArrow =  (ImageView)findViewById(R.id.locked_arrow);
//		videoArrow =  (ImageView)findViewById(R.id.locked_arrow);
//
//		lockedButton.setOnClickListener(new TextView.OnClickListener(){
//			@Override
//			public void onClick(View v) {
//				try {
//					Intent intent = new Intent();
//					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					intent.setClass(getApplicationContext(), VideoListActivity.class);
//					intent.putExtra("isLock", true);
//					startActivity(intent);
////					Intent intent = new Intent();
////					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////					intent.setClass(getApplicationContext(), LockedActivity.class);
////					startActivity(intent);
//				} catch (Exception e) {
//				}
//			}
//
//		});
//		lockedButton.setOnTouchListener(new OnTouchListener(){
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				if(event.getAction() == MotionEvent.ACTION_DOWN)
//				{
//					lockedButton.setBackgroundColor(Color.parseColor("#5f5f5f5f"));
//					lockedButton.setTextColor(getResources().getColor(R.color.text_blue));
//					lockedArrow.setImageResource(R.drawable.next_down);
//
//				}
//				else if(event.getAction() == MotionEvent.ACTION_UP)
//				{
//					lockedButton.setBackgroundColor(Color.parseColor("#00000000"));
//					lockedButton.setTextColor(getResources().getColor(R.color.text_gray));
//					lockedArrow.setImageResource(R.drawable.next);
//				}
//				return false;
//			}
//
//		});
//
//		videoButton.setOnClickListener(new TextView.OnClickListener(){
//			@Override
//			public void onClick(View v) {
//				try {
//					Intent intent = new Intent();
//					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					intent.setClass(getApplicationContext(), VideoListActivity.class);
//					intent.putExtra("isLock", false);
//					startActivity(intent);
////					Intent intent = new Intent();
////					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////					intent.setClass(getApplicationContext(), FileGridListActivity.class);
////					startActivity(intent);
//				} catch (Exception e) {
//				}
//			}
//
//		});
//
//		videoButton.setOnTouchListener(new OnTouchListener(){
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				if(event.getAction() == MotionEvent.ACTION_DOWN)
//				{
//					videoButton.setBackgroundColor(Color.parseColor("#5f5f5f5f"));
//					videoButton.setTextColor(getResources().getColor(R.color.text_blue));
//					videoArrow.setBackgroundResource(R.drawable.next_down);
//				}
//				else if(event.getAction() == MotionEvent.ACTION_UP)
//				{
//					videoButton.setBackgroundColor(Color.parseColor("#00000000"));
//					videoButton.setTextColor(getResources().getColor(R.color.text_gray));
//					videoArrow.setImageResource(R.drawable.next);
//				}
//				return false;
//			}
//
//		});
//
//		returnButton.setOnClickListener(new Button.OnClickListener(){
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent();
//				intent.setAction("preview_window_on");
//				sendBroadcast(intent);
//				finish();
//			}
//		});
//	}


	private Button mBTNBack;
	private ListView mFloderListView;
	private View.OnClickListener mBTNBackClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction("preview_window_on");
				sendBroadcast(intent);
				finish();
		}
	};


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_folderlist);

		Log.e("debug", "oncreate activity");

		setupViews();
	}

	private void setupViews(){
		mBTNBack = (Button) findViewById(R.id.btn_back);
		mFloderListView = (ListView) findViewById(R.id.file_list);
		mBTNBack.setOnClickListener(mBTNBackClickListener);

		FolderAdapter folderAdapter = new FolderAdapter(this, R.layout.layout_folder_list_item);
		mFloderListView.setAdapter(folderAdapter);
		mFloderListView.setOnItemClickListener(onItemClickListener);
	}

	class FolderAdapter extends BaseAdapter{
		private List<String> mList;
		private Context mContext;
		private int mLayoutID;
		public FolderAdapter(Context context, int layoutID){
			mList = new ArrayList<String>();
			Resources resources = context.getResources();
			mList.add(resources.getString(R.string.lock_folder));
			mList.add(resources.getString(R.string.temp_folder));

			mLayoutID = layoutID;
			mContext = context;
		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int position) {
			return mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				convertView = View.inflate(mContext, R.layout.layout_folder_list_item, null);
			}

			initListItem(convertView, position);
			return convertView;
		}

		private void initListItem(View converView ,int position){
			TextView textView = (TextView) converView.findViewById(R.id.tv_folder_name);
			textView.setText(mList.get(position));
		}
	}

	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Log.e("debug", "onItemClick");
			if(id == 0){
				onLockFolderClick();
			}else if(id == 1){
				onTempFolderClick();
			}
		}
	};

	private void  onLockFolderClick(){
		try{
			Intent intent = new Intent();
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setClass(this, VideoListActivity.class);
			intent.putExtra("isLock", true);
			startActivity(intent);
		}catch (Exception e){

		}

	}

	private void onTempFolderClick(){
		try{
			Intent intent = new Intent();
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setClass(this, VideoListActivity.class);
			intent.putExtra("isLock", false);
			startActivity(intent);
		}catch (Exception e){

		}

	}

}