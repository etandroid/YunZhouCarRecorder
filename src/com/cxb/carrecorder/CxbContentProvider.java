package com.cxb.carrecorder;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

public class CxbContentProvider extends ContentProvider {

	private static final int FLAG_RECORDER_STATE = 4;
	private static final int FLAG_RECORDER_IS_SHOW = 5;

	private static final UriMatcher sMatcher;

	static {
		sMatcher = new UriMatcher(UriMatcher.NO_MATCH);

		sMatcher.addURI("provider.cxb.car_recorder", "car_recorder_state",
				FLAG_RECORDER_STATE);
		sMatcher.addURI("provider.cxb.car_recorder", "car_recorder_is_show",
				FLAG_RECORDER_IS_SHOW);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		int result = sMatcher.match(uri);
		MatrixCursor cursor = null;

		switch (result) {

		case FLAG_RECORDER_STATE:// 行车记录仪状态
			cursor = new MatrixCursor(new String[] { "state" });
			cursor.addRow(new Object[] { VideoRecorder.bStart ? 1 : 0 });
			break;
		case FLAG_RECORDER_IS_SHOW:// 行车记录仪预紧界面是否显示中
			cursor = new MatrixCursor(new String[] { "isShow" });
			cursor.addRow(new Object[] { PreviewService.isFullScreenShow ? 1
					: 0 });
			break;
		default:
			break;
		}
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
