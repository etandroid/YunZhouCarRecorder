package com.cxb.carrecorder;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.ad.adas.sdk.IFcwResult;
import com.ad.adas.sdk.ILdwResult;

public class AdasView extends View {

	IFcwResult fcw = null;
	ILdwResult ldw = null;
	Paint paint = new Paint();
	int previewWidth = 1280;
	int previewHeight = 720;
	private int speed = 0;

	public AdasView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.YELLOW);
		paint.setStrokeWidth(2.0F);
		paint.setTextSize(20.0F);
		paint.setAntiAlias(true);

		int offset = 30;
		canvas.drawLine(convertX(640) - offset, convertY(360), convertX(640)
				+ offset, convertY(360), paint);
		canvas.drawLine(convertX(640), convertY(360) - offset, convertX(640),
				convertY(360) + offset, paint);
		// canvas.drawPoint(convertX(640), convertY(360), paint);
		// canvas.drawText(String.format("width=%d,height=%d",
		// getWidth(),getHeight()), 10, 50, paint);
		// canvas.drawText(String.format("Speed=%d", speed), 10, 50, paint);

		if (fcw != null) {
			paint.setColor(Color.BLUE);
			int flag = fcw.getFlag();
			Rect rect = fcw.getCarPos();
			if (flag > 1) {
				paint.setColor(Color.RED);
				canvas.drawText(String.valueOf(fcw.getCrashTime()) + " s",
						convertX((rect.left + rect.right) / 2),
						convertY((rect.top + rect.bottom) / 2), paint);
				if (flag == 2)
					paint.setColor(Color.GREEN);
				if (flag == 3)
					paint.setColor(Color.YELLOW);
				if (flag == 4)
					paint.setColor(Color.RED);
				canvas.drawRect(convertX(rect.left), convertY(rect.top),
						convertX(rect.right), convertY(rect.bottom), paint);
			}
		}

		if (ldw != null) {
			paint.setStyle(Paint.Style.FILL);
			paint.setColor(Color.GREEN);
			int flag = ldw.getFlag();
			List<Point> leftList = ldw.getLeftLanePos();
			List<Point> rightList = ldw.getRightLanePos();
			if (flag != 3) {
				if (flag == 1)
					paint.setColor(Color.RED);
				for (Point point : leftList) {
					canvas.drawPoint(convertX(point.x), convertY(point.y),
							paint);
				}
				paint.setColor(Color.GREEN);
				if (flag == 2)
					paint.setColor(Color.RED);
				for (Point point : rightList) {
					canvas.drawPoint(convertX(point.x), convertY(point.y),
							paint);
				}
				// canvas.drawLine(convertX(list.get(0).x),
				// convertY(list.get(0).y), convertX(list.get(1).x),
				// convertY(list.get(1).y), paint);
				// canvas.drawLine(convertX(list.get(2).x),
				// convertY(list.get(2).y), convertX(list.get(3).x),
				// convertY(list.get(3).y), paint);
			}
		}
	}

	public void updateFcw(IFcwResult fcw) {
		this.fcw = fcw;
		postInvalidate();
	}

	public void updateLdw(ILdwResult ldw) {
		this.ldw = ldw;
		postInvalidate();
	}

	public void updateSpeed(int speed) {
		this.speed = speed;
		postInvalidate();
	}

	private int convertX(int x) {
		return x * getWidth() / previewWidth;
	}

	private int convertY(int y) {
		return y * getHeight() / previewHeight;
	}

}
