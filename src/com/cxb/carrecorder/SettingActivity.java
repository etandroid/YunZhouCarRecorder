package com.cxb.carrecorder;

import org.xutils.x;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends Activity {

	private Button muteButton = null;
	private Button adasButton = null;
	private ViewGroup adasLayout;

	private View formatButton = null;
	private Button returnButton = null;

	private int mRecordTime = 1;
	private int mResolution = 1;
	private boolean bMute = true; // 是否静音
	private String[] items = null;

	private int externalSDsize = 0;
	private String[] recordTimeList = new String[3];
	private static int times = 0;

	private TextView mTimeView = null;
	private ImageView mNextView = null;
	private ImageView mPrevView = null;

	private PersistUtils persistUtils;
	private VolumnUtils volumnUtils;
	private AlertDialog formatSDAlertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setting_activity);

		persistUtils = new PersistUtils(this);
		volumnUtils = new VolumnUtils(this);
		mTimeView = (TextView) findViewById(R.id.time_set);
		adasLayout = (ViewGroup) findViewById(R.id.rl_adas);
		mRecordTime = persistUtils.getRecordTime();
		String strTime = String.format("%02d:%02d", 0, mRecordTime);
		mTimeView.setText(strTime);

		mPrevView = (ImageView) findViewById(R.id.prev_time);
		mPrevView.setOnClickListener(new ImageView.OnClickListener() {
			public void onClick(View v) {
				mRecordTime -= 2;
				if (mRecordTime <= 0)
					mRecordTime = 10;
				String strTime = String.format("%02d:%02d", 0, mRecordTime);
				mTimeView.setText(strTime);
				persistUtils.setRecordTime(mRecordTime);
			}
		});
		mPrevView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					mPrevView.setImageResource(R.drawable.prev_time_down);

				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					mPrevView.setImageResource(R.drawable.prev_time);
				}
				return false;
			}

		});

		mNextView = (ImageView) findViewById(R.id.next_time);
		mNextView.setOnClickListener(new ImageView.OnClickListener() {
			public void onClick(View v) {
				mRecordTime += 2;
				if (mRecordTime > 10)
					mRecordTime = 2;
				String strTime = String.format("%02d:%02d", 0, mRecordTime);
				mTimeView.setText(strTime);
				persistUtils.setRecordTime(mRecordTime);
			}
		});
		mNextView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					mNextView.setImageResource(R.drawable.next_time_down);

				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					mNextView.setImageResource(R.drawable.next_time);
				}
				return false;
			}

		});

		muteButton = (Button) findViewById(R.id.muteButton);

		if (!persistUtils.getRecordAudioEnable()) {
			muteButton.setBackgroundResource(R.drawable.close);
		} else {
			muteButton.setBackgroundResource(R.drawable.open);
		}

		muteButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean isRecordAudio = persistUtils.getRecordAudioEnable();
				if (isRecordAudio) {
					muteButton.setBackgroundResource(R.drawable.close);
				} else {
					muteButton.setBackgroundResource(R.drawable.open);
				}
				persistUtils.setRecordAudioEnable(!isRecordAudio);

			}
		});

		if (!Utils.getAdasSupport()) {
			adasLayout.setVisibility(View.GONE);
		}
		adasButton = (Button) findViewById(R.id.adas);

		if (!persistUtils.getAdasEnable()) {
			adasButton.setBackgroundResource(R.drawable.close);
		} else {
			adasButton.setBackgroundResource(R.drawable.open);
		}

		adasButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean adasEnable = persistUtils.getAdasEnable();
				if (adasEnable) {
					adasButton.setBackgroundResource(R.drawable.close);
				} else {
					adasButton.setBackgroundResource(R.drawable.open);
				}
				persistUtils.setAdasEnable(!adasEnable);

			}
		});

		returnButton = (Button) findViewById(R.id.return_button);
		returnButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction("preview_window_on");
				sendBroadcast(intent);
				finish();
			}
		});

		formatButton = (View) findViewById(R.id.tv_format);
		formatButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (PreviewService.isSDExists(SettingActivity.this)) {
					showFormatSDComfirmDialog();

				} else {
					Toast.makeText(SettingActivity.this, R.string.cardverify, 1)
							.show();
				}
			}
		});
	}

	private void showFormatSDComfirmDialog() {
		if (formatSDAlertDialog == null) {
			formatSDAlertDialog = new AlertDialog.Builder(this)
					.setTitle(R.string.worning)
					.setMessage(R.string.formatverify)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									sendBroadcast(new Intent(
											PreviewService.ACTION_START_FORMAT_SDCARD));
									x.task().postDelayed(new Runnable() {

										@Override
										public void run() {
											volumnUtils
													.formatVolumn(VolumnUtils.VOLUMN_EXTERNAL_SDCARD_PATH);

										}
									}, 2000);

								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();

								}
							}).create();
		}
		if (!formatSDAlertDialog.isShowing()) {
			formatSDAlertDialog.show();
		}
	}

}
