package com.cxb.carrecorder;

import org.xutils.x;

import android.app.Application;

public class RecorderApplication extends Application {

	@Override
	public void onCreate() {
		x.Ext.init(this);
		x.Ext.setDebug(false);
		super.onCreate();
	}
}
