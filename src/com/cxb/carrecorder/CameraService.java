package com.cxb.carrecorder;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class CameraService extends Service {
	private ICameraCallBack mCameraCallBack;

	@Override
	public void onCreate() {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	private final ICameraService.Stub mBinder = new ICameraService.Stub() {

		@Override
		public void unregisterCallBack() throws RemoteException {
			// TODO Auto-generated method stub
			mCameraCallBack = null;
		}

		@Override
		public void registerCallBack(ICameraCallBack cb) throws RemoteException {
			// TODO Auto-generated method stub
			mCameraCallBack = cb;
		}

		@Override
		public void getCameraFrame(byte[] data) throws RemoteException {
			// TODO Auto-generated method stub
			try {
				if (mCameraCallBack != null) {
					mCameraCallBack.onCameraFrameCallBack(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};
}
