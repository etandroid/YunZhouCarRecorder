package com.cxb.carrecorder;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.IBinder;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.ad.adas.sdk.AdasService;
import com.ad.adas.sdk.DebugConfig;
import com.ad.adas.sdk.IAdasBinder;
import com.ad.adas.sdk.IAdasCallback;
import com.ad.adas.sdk.IFcwResult;
import com.ad.adas.sdk.ILdwResult;

public class AdasTestActivity extends Activity {

	private static final String TAG = "AdasDemo";
	private SurfaceView mCameraPreView;
	private TextView mFcwCallbackText;
	private TextView mLdwCallbackText;
	private Camera mCamera;
	private SurfaceHolder mSurfaceHolder;
	private int PREVIEW_WIDTH = 1280;
	private int PREVIEW_HEIGHT = 720;
	
	private AdasView adasView;
	
	private IAdasBinder adasService;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		setContentView(R.layout.adas_test);

		mFcwCallbackText = (TextView) findViewById(R.id.fcwcallback);
		mLdwCallbackText = (TextView) findViewById(R.id.ldwcallback);

		adasView = (AdasView) findViewById(R.id.adasView);
		
		mCameraPreView = (SurfaceView) findViewById(R.id.preview);
		mSurfaceHolder = mCameraPreView.getHolder();
		mSurfaceHolder.addCallback(mSurfaceCallback);
		
		bindService(new Intent(this,AdasService.class), mServiceConnection, Service.BIND_AUTO_CREATE);
	}
	
	IAdasCallback adasCallback = new IAdasCallback() {

		@Override
		public void onFcw(final IFcwResult result) {
			adasView.updateFcw(result);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mFcwCallbackText != null)
						mFcwCallbackText.setText(String.format("FCW:%d,%d,%.2f", result.getFlag(), result.getDistance(), result.getCrashTime()));
				}
			});
		}

		@Override
		public void onLdw(final ILdwResult result) {
			adasView.updateLdw(result);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(mLdwCallbackText!=null)
						mLdwCallbackText.setText(String.format("LDW:%d", result.getFlag()));
				}
			});
		}

		@Override
		public void onSpeedUpdate(int arg0) {
			if(!DebugConfig.isTest){
				adasView.updateSpeed(arg0);
			}else{
				adasView.updateSpeed(DebugConfig.speed);
			}
		}
	};
	
	ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			adasService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service)
		{
			adasService = (IAdasBinder) service;
			if(adasService!=null){
				adasService.setCallback(adasCallback);
				adasService.init(1280, 720, 871, 1300, 640, 360,1800,0,1700,2,2);
			}
		}
	};

	private void openCamera() {
		try {
			mCamera = Camera.open();
			Parameters params = mCamera.getParameters();
			params.setPreviewSize(PREVIEW_WIDTH, PREVIEW_HEIGHT);
			mCamera.setParameters(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private SurfaceHolder.Callback mSurfaceCallback = new Callback() {

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			if (mCamera == null) {
				openCamera();
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			try {
				mCamera.setPreviewDisplay(holder);
				startPreview();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	};

	AtomicBoolean atomicBoolean = new AtomicBoolean(false);
	long time = System.currentTimeMillis();
	
	private PreviewCallback mCameraCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, final Camera camera) {
			if(adasService!=null)
				adasService.detect(data, 1280, 720);
//			final byte[] nv21 = data.clone();
//			if(System.currentTimeMillis()>time){
//				time += 10 * 1000L;
//				runOnUiThread(new Runnable() {
//					public void run() {
//							if(speed==Integer.MAX_VALUE) speed = 0;
//							speed ++;
//							try {
//								if(adasService!=null)
//									adasService.detect(nv21, PREVIEW_WIDTH, PREVIEW_HEIGHT, speed, 2);
//							} catch (RemoteException e) {
//								e.printStackTrace();
//							} finally{
//							}
//					}
//				});
//			}
		}
	};


	private synchronized void startPreview() {
		if (mCamera != null) {
			mCamera.startPreview();
			mCamera.setPreviewCallback(mCameraCallback);
		}
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(mServiceConnection);
	}
	
	public void onClickFinish(View view){
		finish();
	}
	
	public void onClickTest(View view){
		DebugConfig.isTest = !DebugConfig.isTest;
		if(DebugConfig.isTest){
			adasView.updateSpeed(DebugConfig.speed);
		}else{
			adasView.updateSpeed(0);
		}
	}
}
