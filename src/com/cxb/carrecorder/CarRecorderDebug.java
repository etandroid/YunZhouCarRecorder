package com.cxb.carrecorder;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



import android.util.Log;

public class CarRecorderDebug {

	public final static boolean DEBUG = false;
	public final static boolean ISDEBUG = false;
	public final static boolean PHOTOISDEBUG = false;
	private static DateFormat formatter = new SimpleDateFormat("HH:mm:ss");

 
	public static void printf(String tag, String msg) {
		if (tag != null && msg != null) {
			Log.i(tag, msg);
		}

	}


	public static void printfRECORDERLog(String content) {
		if (ISDEBUG) {
			try {
				FileWriter writer = new FileWriter("/mnt/sdcard/record.log",
						true);
				StringBuilder sb = new StringBuilder();
				sb.append(content);
				sb.append(" --- ");
				sb.append(formatter.format(new Date()));
				sb.append('\n');
				writer.write(sb.toString());
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void printfPhotoLog(String content) {
		if (PHOTOISDEBUG) {
			try {
				FileWriter writer = new FileWriter(
						"/mnt/sdcard/photoUpload.log", true);
				StringBuilder sb = new StringBuilder();
				sb.append(content);
				sb.append(" --- ");
				sb.append(formatter.format(new Date()));
				sb.append('\n');
				writer.write(sb.toString());
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void printfPhotoLog(Exception ex) {
		if (PHOTOISDEBUG) {
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		ex.printStackTrace(printWriter);
		Throwable cause = ex.getCause();
		while (cause != null) {
			cause.printStackTrace(printWriter);
			cause = cause.getCause();
		}
		printWriter.close();
		String result = writer.toString();
		try {
			FileWriter fwriter = new FileWriter("/mnt/sdcard/photoUpload.log",
					true);
			fwriter.write(result);
			fwriter.flush();
			fwriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	}
}
