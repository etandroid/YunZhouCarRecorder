package com.cxb.carrecorder;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PersistUtils {

	private Context mContext;

	private SharedPreferences spPreferences;

	public PersistUtils(Context context) {
		mContext = context;
		spPreferences = context
				.getSharedPreferences("dvr", Context.MODE_APPEND);
	}

	public void setRecordAudioEnable(boolean enable) {
		Editor editor = spPreferences.edit();
		editor.putBoolean("record_enale", enable);
		editor.commit();
	}

	public boolean getAdasEnable() {
		return spPreferences.getBoolean("adas_enale", false);
	}

	public void setAdasEnable(boolean enable) {
		Editor editor = spPreferences.edit();
		editor.putBoolean("adas_enale", enable);
		editor.commit();
	}

	public boolean getRecordAudioEnable() {
		return spPreferences.getBoolean("record_enale", false);
	}

	public void setRecordTime(int time) {
		Editor editor = spPreferences.edit();
		editor.putInt("record_time", time);
		editor.commit();
	}

	public int getRecordTime() {
		return spPreferences.getInt("record_time", 2);
	}

	public String getAdasLicence() {
		return spPreferences.getString("AdasLicence", null);
	}

	public void setPreviewPushEnable(boolean enable) {
		RootContext
				.runCommandForResult("setprop persist.previewcallback.enable "
						+ (enable ? "1" : "0"));
	}

}
