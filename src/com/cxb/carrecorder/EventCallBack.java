package com.cxb.carrecorder;

interface IShareCallBack {
	void onVideoComplete(String strFile);
	void onPictureShareComplete(String strFile);
}

public class EventCallBack {
	private IShareCallBack mCallBack; 
	public void onPictureShareComplete(String strFile) {
		mCallBack.onPictureShareComplete(strFile);
	}
	
	public void onVideoComplete(String strFile) {
		mCallBack.onVideoComplete(strFile);
	}

    public void setCallBack(IShareCallBack callback) {  
        this.mCallBack = callback;  
    }  

}
