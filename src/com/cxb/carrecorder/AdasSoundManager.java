package com.cxb.carrecorder;

import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class AdasSoundManager {
	
//	private static final int TYPE_FCW_RED = 1;
	private static final int TYPE_FCW_HRED = 2;
	private static final int TYPE_LDW = 4;
	private static final int TYPE_OTHER = 14;
	
	private SoundPool soundPool;
	private HashMap<Integer, Integer> sounds;  
	
	public AdasSoundManager(Context context) {
		soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
		sounds = new HashMap<Integer, Integer>();
		sounds.put(TYPE_FCW_HRED, soundPool.load(context, R.raw.fcw_h, 1));
		sounds.put(TYPE_LDW, soundPool.load(context, R.raw.ldw, 1));
		sounds.put(TYPE_OTHER, soundPool.load(context, R.raw.other, 1));
	}

	public void playFcwRed() {
		play(TYPE_OTHER);
	}

	public void playFcwHRed() {
		play(TYPE_FCW_HRED);
	}

	public void playLdw() {
		play(TYPE_LDW);
	}

	private void play(int type){
		soundPool.play(sounds.get(type), 1, 1, 1, 0, 1);
	}
}
