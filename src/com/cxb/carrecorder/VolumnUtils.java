package com.cxb.carrecorder;

import java.io.File;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.StatFs;

public class VolumnUtils {

	private Context mContext;

	public final static String VOLUMN_EXTERNAL_SDCARD_PATH = "/storage/sdcard1";// �ⲿ�洢·��
	public final static String VOLUMN_INNER_SDCARD_PATH = "/storage/sdcard0";// ���ô洢·��

	public VolumnUtils(Context context) {
		mContext = context;
	}

	public  void formatVolumn(String path) {
		Intent intent = new Intent();
		intent.setComponent(new ComponentName("android",
				"com.android.internal.os.storage.ExternalStorageFormatter"));
		intent.putExtra("path", path);
		mContext.startService(intent);
	}
	
    public void deleteFile(File file) {
    			 if(!file.exists()){
    				 return;
    			 }
		          if (file.isFile()) {  
		              file.delete();  
		              return;  
		          }  
		    
		          if(file.isDirectory()){  
		              File[] childFiles = file.listFiles();  
		              if (childFiles == null || childFiles.length == 0) {  
		                  file.delete();  
		                  return;  
		              }  
		        
		              for (int i = 0; i < childFiles.length; i++) {  
		            	  deleteFile(childFiles[i]);  
		              }  
		              //file.delete();  
		          }  
		      } 

	public static long getAvailableMemorySize(String path) {
		if (!new File(path).exists()) {
			return -1;
		}
		StatFs stat = new StatFs(path);
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getBlockCount();
		// long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	public static boolean isSDExist() {
		
		long sdcard1=getAvailableMemorySize(VOLUMN_EXTERNAL_SDCARD_PATH);
		long sdcard2=getAvailableMemorySize(VOLUMN_INNER_SDCARD_PATH);
		return sdcard1 > 0
				&& sdcard2 > 0;
	}
}
