package com.cxb.carrecorder;

interface ICameraCallBack {
    void onCameraFrameCallBack(in byte[] data);
}