package com.cxb.carrecorder;
import com.cxb.carrecorder.ICameraCallBack;

interface ICameraService {
    void getCameraFrame(in byte[] data);
    void registerCallBack(ICameraCallBack cb);
    void unregisterCallBack();
}
